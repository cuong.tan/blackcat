/**
 * @format
 */

import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {NativeModules} from 'react-native';

import ListingNavigation from 'src/navigation/ListingNavigation';
import LoginNavigation from 'src/navigation/LoginNavigation';
import {store, persistor} from 'src/redux/store';
import screenIds from './src/assets/constants/screens';

const AppCon = createAppContainer(
  createSwitchNavigator(
    {
      [screenIds.LISTING_NAV]: ListingNavigation,
      [screenIds.LOGIN_NAV]: LoginNavigation,
    },
    {
      initialRouteName: screenIds.LOGIN_NAV,
    },
  ),
);

class App extends Component {
  turnOnDebugger = () => {
    if (__DEV__) {
      //NativeModules.DevSettings.setIsDebuggingRemotely(true);
      //NativeModules.DevMenu.debugRemotely(true);
    }
  };

  componentDidMount() {
    this.turnOnDebugger();
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppCon />
        </PersistGate>
      </Provider>
    );
  }
}

AppRegistry.registerComponent(appName, () => App);
