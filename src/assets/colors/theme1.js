export default {
  primary: '#8560A9',
  primaryDark: '#453257',
  selection: '#D5EF7F',
};
