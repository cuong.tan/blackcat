/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStackNavigator} from 'react-navigation-stack';

import Login from 'src/screens/Login';
import screenIds from '../assets/constants/screens';
import ItemDetails from 'src/screens/ItemDetails';

const ListingNavigation = createStackNavigator(
  {
    [screenIds.LISTING]: {
      screen: Login,
    },
  },
  {
    initialRouteName: screenIds.LISTING,
  },
);

export default ListingNavigation;
