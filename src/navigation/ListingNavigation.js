/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStackNavigator} from 'react-navigation-stack';

import Listing from 'src/screens/Listing';
import screenIds from '../assets/constants/screens';
import ItemDetails from 'src/screens/ItemDetails';
import UserProfile from 'src/screens/UserProfile';

const ListingNavigation = createStackNavigator(
  {
    [screenIds.LISTING]: {
      screen: Listing,
    },
    [screenIds.ITEM_DETAILS]: {
      screen: ItemDetails,
    },
    [screenIds.USER_PROFILE]: {
      screen: UserProfile,
    },
  },
  {
    initialRouteName: screenIds.LISTING,
  },
);

export default ListingNavigation;
