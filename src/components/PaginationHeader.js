import React, {Component} from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

class PaginationHeader extends Component {
  static propTypes = {
    goToTab: PropTypes.func,
    tabIndex: PropTypes.number,
    titles: PropTypes.array,
  };

  static defaultProps = {
    goToTab: () => {},
    tabIndex: 0,
    titles: [],
  };

  render() {
    const {titles = []} = this.props;
    return (
      <View style={styles.tabBar}>
        {titles.map((title, index) => {
          return (
            <React.Fragment key={index}>
              {index > 0 && <View key={2 * index} style={styles.verticalBar} />}
              <TouchableOpacity
                key={2 * index + 1}
                style={styles.tabTitle}
                onPress={() => {
                  if (typeof this.props.goToTab === 'function') {
                    this.props.goToTab(index);
                  }
                }}>
                <Text
                  style={
                    this.props.tabIndex === index
                      ? styles.selectedTabText
                      : styles.tabText
                  }>
                  {title}
                </Text>
              </TouchableOpacity>
            </React.Fragment>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabBar: {
    width: '100%',
    height: 49,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#D8D8D8',
    backgroundColor: '#fff',
  },
  tabTitle: {
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  verticalBar: {
    height: 12,
    width: 1,
    backgroundColor: '#D8D8D8',
  },
  tabText: {
    fontSize: 14,
    color: '#8C8C8C',
  },
  selectedTabText: {
    fontSize: 14,
    color: '#AECB4F',
  },
});

export default PaginationHeader;
