import React, {Component} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import themeColor from 'src/assets/colors/theme1';
import SearchButton from 'src/components/SearchButton';
import DateInputBox from 'src/components/DateInputBox';

class Filter extends Component {
  static propTypes = {
    byDate: PropTypes.array,
    byChannel: PropTypes.array,
  };

  static defaultProps = {
    byDate: [],
    byChannel: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      dateFilter: {},
      channelFilter: {},
      dateRange: {},
    };
  }

  selectDateFilter = filter => () => {
    this.setState({
      dateFilter: filter,
    });
  };

  selectChannelFilter = channel => () => {
    this.setState({
      channelFilter: channel,
    });
  };

  onDateChanged = ({fromDate, toDate}) => {
    this.setState({
      dateRange: {fromDate, toDate},
    });
  };

  renderDateFilter = () => {
    return (
      <View style={styles.filterContainer}>
        <View style={styles.titleWrapper}>
          <Text style={styles.titleText}>DATE</Text>
        </View>
        <View style={styles.filterList}>
          {this.props.byDate.map(dateFilter => (
            <TouchableOpacity
              onPress={this.selectDateFilter(dateFilter)}
              key={dateFilter.id}
              style={
                this.state.dateFilter.id === dateFilter.id
                  ? styles.selectedFilter
                  : styles.dateFilter
              }>
              <Text
                style={
                  this.state.dateFilter.id === dateFilter.id
                    ? styles.selectedFilterLabel
                    : styles.filterLabel
                }>
                {dateFilter.label}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  };

  renderChannelFilter = () => {
    return (
      <View>
        <View style={styles.filterContainer}>
          <View style={styles.titleWrapper}>
            <Text style={styles.titleText}>CHANNEL</Text>
          </View>
          <View style={styles.filterList}>
            {this.props.byChannel.map(channelFilter => (
              <TouchableOpacity
                onPress={this.selectChannelFilter(channelFilter)}
                key={channelFilter.id}
                style={[
                  this.state.channelFilter.id === channelFilter.id
                    ? styles.selectedFilter
                    : styles.channelFilter,
                  {marginRight: 4},
                ]}>
                <Text
                  style={
                    this.state.channelFilter.id === channelFilter.id
                      ? styles.selectedFilterLabel
                      : styles.filterLabel
                  }>
                  {channelFilter.label}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </View>
    );
  };

  render() {
    const {dateFilter, channelFilter, dateRange} = this.state;

    return (
      <View style={styles.filter}>
        <SafeAreaView style={{flex: 1}}>
          {this.renderDateFilter()}
          <DateInputBox
            hidden={dateFilter.label !== 'LATER'}
            onDateChanged={this.onDateChanged}
          />
          {this.renderChannelFilter()}
        </SafeAreaView>
        <SearchButton
          disabled={
            typeof dateFilter.id === 'undefined' &&
            typeof channelFilter.id === 'undefined'
          }
          onPress={() =>
            this.props.onFilter({dateFilter, channelFilter, dateRange})
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  channelFilter: {
    height: 24,
    paddingHorizontal: 4,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#fff',
    borderRadius: 12,
    marginBottom: 7,
  },
  filterList: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginTop: 16,
  },
  filterContainer: {
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 26,
    marginBottom: 12,
  },
  filter: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: themeColor.primaryDark,
  },
  titleWrapper: {
    borderBottomWidth: 1,
    borderColor: '#8560A9',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    fontSize: 12,
    color: '#AC8EC9',
  },
  selectedFilter: {
    height: 24,
    paddingHorizontal: 4,
    backgroundColor: themeColor.selection,
    borderColor: themeColor.selection,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
  },
  dateFilter: {
    height: 24,
    paddingHorizontal: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filterLabel: {
    color: '#fff',
  },
  selectedFilterLabel: {
    color: themeColor.primaryDark,
  },
});

export default Filter;
