import React, {Component} from 'react';
import {View, Keyboard, Animated} from 'react-native';
import PropTypes from 'prop-types';

class KeyboardPaddingView extends Component {
  static propTypes = {
    initialHeight: PropTypes.number,
    customPadding: PropTypes.number,
  };

  static defaultProps = {
    initialHeight: 0,
    customPadding: 0,
  };

  constructor(props) {
    super(props);

    this.state = {
      paddingHeight: new Animated.Value(props.initialHeight),
    };
  }

  componentDidMount() {
    this.keyboardDidShow = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow,
    );
    this.keyboardDidHide = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide,
    );
  }

  componentWillUnmount() {
    if (this.keyboardDidShow) {
      this.keyboardDidShow.remove();
    }

    if (this.keyboardDidHide) {
      this.keyboardDidHide.remove();
    }
  }

  keyboardDidShow = event => {
    Animated.timing(this.state.paddingHeight, {
      toValue: event.endCoordinates.height,
      duration: 250,
    }).start();
  };

  keyboardDidHide = () => {
    Animated.timing(this.state.paddingHeight, {
      toValue: this.props.initialHeight,
      duration: 250,
    }).start();
  };

  render() {
    const {customPadding} = this.props;
    const {paddingHeight} = this.state;

    return <Animated.View style={{height: paddingHeight}} />;
  }
}

export default KeyboardPaddingView;
