import React, {Component} from 'react';
import {View, StyleSheet, TextInput, Text} from 'react-native';
import {SvgXml} from 'react-native-svg';
import PropTypes from 'prop-types';

import dateFromIcon from 'src/assets/svgs/date-from.svg';
import dateToIcon from 'src/assets/svgs/date-to.svg';
import themeColor from 'src/assets/colors/theme1';
import FormattedInputBox from 'src/components/FormattedInputBox';

class DateInputBox extends Component {
  static propTypes = {
    onDateChanged: PropTypes.func,
    hidden: PropTypes.bool,
  };

  static defaultProps = {
    onDateChanged: () => {},
    hidden: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      fromDate: '',
      toDate: '',
    };
  }

  onDateChanged = () => {
    this.props.onDateChanged({
      fromDate: this.state.fromDate,
      toDate: this.state.toDate,
    });
  };

  updateFromDate = date => {
    this.setState(
      {
        fromDate: date,
      },
      this.onDateChanged,
    );
  };

  updateToDate = date => {
    this.setState(
      {
        toDate: date,
      },
      this.onDateChanged,
    );
  };

  render() {
    if (this.props.hidden) {
      return <View style={styles.dateInputBox} />;
    }

    return (
      <View style={styles.dateInputBox}>
        <View style={styles.arrow} transform={[{rotateZ: '45deg'}]} />
        <View style={styles.inputBoxWrapper}>
          <View style={styles.half}>
            <SvgXml
              style={styles.icon}
              xml={dateFromIcon}
              width={16}
              height={16}
              fill={themeColor.selection}
            />
            <FormattedInputBox onChangeText={this.updateFromDate} />
          </View>
          <View style={styles.separator} />
          <View style={styles.half}>
            <SvgXml
              style={styles.icon}
              xml={dateToIcon}
              width={16}
              height={16}
              fill={themeColor.selection}
            />
            <FormattedInputBox onChangeText={this.updateToDate} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  arrow: {
    height: 12,
    width: 12,
    backgroundColor: '#fff',
    right: 44,
    top: 0,
    position: 'absolute',
  },
  inputBoxWrapper: {
    height: 30,
    backgroundColor: '#fff',
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 8,
  },
  dateInputBox: {
    height: 34,
    flexDirection: 'column-reverse',
  },
  half: {
    height: 32,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    marginRight: 8,
  },
  separator: {
    backgroundColor: '#ccc',
    width: 10,
    height: 1,
    marginHorizontal: 8,
  },
});

export default DateInputBox;
