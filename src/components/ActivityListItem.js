import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';

import themeColor from 'src/assets/colors/theme1';
import {SvgXml} from 'react-native-svg';
import user from 'src/assets/svgs/user.svg';
import time from 'src/assets/svgs/time.svg';
import check from 'src/assets/svgs/check.svg';
import checkOutline from 'src/assets/svgs/check-outline.svg';
import like from 'src/assets/svgs/like.svg';

class ActivityListItem extends Component {
  static propTypes = {
    data: PropTypes.any,
    onPress: PropTypes.func,
    onGoingPress: PropTypes.func,
  };

  static defaultProps = {
    data: {},
    onPress: () => {},
    onGoingPress: () => {},
  };

  renderItemInfo = () => {
    const {data: item} = this.props;

    return (
      <View style={styles.itemInfo}>
        <View style={styles.nameContainer}>
          <SvgXml xml={user} width={20} height={20} />
          <View style={{width: 6}} />
          <Text style={{color: '#222'}}>{item.full_name}</Text>
        </View>
        <View style={styles.channelName}>
          <Text style={{color: themeColor.primary}}>{item.channel_name}</Text>
        </View>
      </View>
    );
  };

  renderTimeContainer = () => {
    const {data: item} = this.props;

    return (
      <View style={styles.timeContainer}>
        <SvgXml fill={themeColor.primary} xml={time} width={20} height={20} />
        <View style={{width: 4}} />
        <Text style={{color: themeColor.primary}}>{`${item.start_time} - ${
          item.end_time
        }`}</Text>
      </View>
    );
  };

  renderContent = () => {
    const {data: item} = this.props;

    return (
      <View style={{paddingVertical: 4}}>
        <Text style={{fontSize: 18, color: '#575757'}}>
          {item.content.length < 300
            ? item.content
            : item.content.substring(0, 300) + '...'}
        </Text>
      </View>
    );
  };

  renderUserResponse = () => {
    const {data: item} = this.props;

    return (
      <View style={styles.userResponse}>
        <TouchableOpacity onPress={this.props.onGoingPress}>
          {item.is_going ? this.renderIAmGoing() : this.renderGoing(item)}
        </TouchableOpacity>
        <View style={{width: 40}} />
        <TouchableOpacity>
          {item.is_like ? this.renderILikeIt() : this.renderLike(item)}
        </TouchableOpacity>
      </View>
    );
  };

  renderIAmGoing = () => {
    return (
      <View style={styles.interactWrapper}>
        <SvgXml
          style={styles.interactIcon}
          fill={themeColor.selection}
          xml={check}
          width={16}
          height={16}
        />
        <Text>I am going!</Text>
      </View>
    );
  };

  renderGoing = () => {
    const {data: item} = this.props;

    return (
      <View style={styles.interactWrapper}>
        <SvgXml
          style={styles.interactIcon}
          fill={themeColor.primary}
          xml={checkOutline}
          width={16}
          height={16}
        />
        <Text style={{color: themeColor.primary}}>
          {item.going_count || 0} Going
        </Text>
      </View>
    );
  };

  renderILikeIt = () => {
    return (
      <View style={styles.interactWrapper}>
        <SvgXml
          style={styles.interactIcon}
          fill="#FF5C5C"
          xml={like}
          width={16}
          height={16}
        />
        <Text>I like it</Text>
      </View>
    );
  };

  renderLike = () => {
    const {data: item} = this.props;

    return (
      <View style={styles.interactWrapper}>
        <SvgXml
          style={styles.interactIcon}
          fill={themeColor.primary}
          xml={checkOutline}
          width={16}
          height={16}
        />
        <Text style={{color: themeColor.primary}}>
          {item.like_count || 0} {item.like_count < 2 ? 'Like' : 'Likes'}
        </Text>
      </View>
    );
  };

  render() {
    const {id = '', isLoadingPage = false, title = ''} = this.props.data;

    if (isLoadingPage) {
      return (
        <View
          style={{
            width: '100%',
            height: 64,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={styles.listItem}>
          {this.renderItemInfo()}
          <Text style={styles.title} numberOfLines={2}>
            {title}
          </Text>
          {this.renderTimeContainer()}
          {this.renderContent()}
          {this.renderUserResponse()}
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  userResponse: {
    paddingVertical: 4,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  listItem: {
    padding: 20,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    width: '100%',
    fontSize: 26,
    paddingVertical: 2,
    color: themeColor.primaryDark,
  },
  itemInfo: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 4,
  },
  nameContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  channelName: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    height: 20,
    borderRadius: 10,
    paddingHorizontal: 4,
    borderColor: themeColor.primary,
  },
  timeContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignSelf: 'stretch',
    paddingVertical: 4,
  },
  interactIcon: {
    marginRight: 4,
  },
  interactWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ActivityListItem;
