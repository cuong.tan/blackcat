import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import {SvgXml} from 'react-native-svg';
import themeColor from 'src/assets/colors/theme1';
import noActivity from 'src/assets/svgs/no-activity.svg';

class ActivityList extends Component {
  static propTypes = {
    activities: PropTypes.array,
    isWaiting: PropTypes.bool,
    renderItem: PropTypes.func,
    loadNextPage: PropTypes.func,
  };

  static defaultProps = {
    activities: [],
    isWaiting: false,
    renderItem: () => {},
    loadNextPage: () => {},
  };

  renderWaiting = () => {
    return (
      <View
        style={{
          width: '100%',
          height: 300,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <ActivityIndicator />
      </View>
    );
  };

  renderEmptyList = () => {
    return (
      <View
        style={{
          width: '100%',
          height: 350,
          backgroundColor: '#fff',
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <View style={{height: '25%'}} />
        <SvgXml
          style={styles.noActivity}
          fill={themeColor.primary}
          xml={noActivity}
          width={72}
          height={72}
        />
        <Text style={styles.notFoundText}>No activity</Text>
        <Text style={styles.notFoundText}>found</Text>
      </View>
    );
  };

  render() {
    if (this.props.isWaiting) {
      return this.renderWaiting();
    }

    if (this.props.activities.length === 0) {
      return this.renderEmptyList();
    }

    return (
      <FlatList
        {...this.props}
        style={styles.flatList}
        data={this.props.activities}
        renderItem={this.props.renderItem}
        keyExtractor={(_, index) => index.toString()}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        onEndReachedThreshold={0}
        onEndReached={this.props.loadNextPage}
      />
    );
  }
}

const styles = StyleSheet.create({
  notFoundText: {
    fontSize: 16,
    color: '#ccc',
  },
  noActivity: {
    marginBottom: 12,
    opacity: 0.3,
  },
  flatList: {
    backgroundColor: '#fff',
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#c9c9c9',
    marginLeft: 20,
  },
});

export default ActivityList;
