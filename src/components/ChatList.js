import React, {Component} from 'react';
import {FlatList, View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {SvgXml} from 'react-native-svg';

import userIcon from 'src/assets/svgs/user.svg';
import replyIcon from 'src/assets/svgs/reply.svg';
import themeColor from 'src/assets/colors/theme1';

class ChatList extends Component {
  renderItem = ({item = {}, index}) => {
    return (
      <View style={styles.chatMessage}>
        <SvgXml
          style={{marginRight: 8}}
          xml={userIcon}
          width={32}
          height={32}
          fill="#ccc"
        />
        <View stlye={styles.contentWrapper}>
          <View style={styles.userInfo}>
            <Text style={styles.userFullName}>{item.full_name}</Text>
            <Text style={styles.postedTime}>{item.posted_time}</Text>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginRight: 40,
            }}>
            <Text style={styles.content}>{item.content}</Text>
          </View>
        </View>
        <TouchableOpacity
          style={styles.replyTouchable}
          onPress={() => {
            if (typeof this.props.onReplyTo === 'function') {
              this.props.onReplyTo(item);
            }
          }}>
          <SvgXml
            xml={replyIcon}
            width={16}
            height={16}
            fill={themeColor.selection}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const {conversations = [], ...rest} = this.props;

    return (
      <FlatList
        keyExtractor={(_, index) => `${index}`}
        renderItem={this.renderItem}
        data={conversations}
        {...rest}
      />
    );
  }
}

const styles = StyleSheet.create({
  replyTouchable: {
    position: 'absolute',
    top: 12,
    right: 8,
  },
  chatMessage: {
    flexDirection: 'row',
    paddingTop: 16,
  },
  contentWrapper: {
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  userInfo: {
    flexDirection: 'row',
    marginBottom: 8,
    alignItems: 'baseline',
  },
  userFullName: {
    fontSize: 16,
    color: themeColor.primary,
    marginRight: 8,
  },
  postedTime: {
    fontSize: 10,
    color: '#BABABA',
  },
  content: {
    fontSize: 14,
    color: '#67616D',
    marginRight: 8,
  },
});

export default ChatList;
