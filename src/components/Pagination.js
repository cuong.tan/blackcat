import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import PaginationHeader from 'src/components/PaginationHeader';

class Pagination extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isRendered: this.props.children.map(_ => false),
      tabIndex: 0,
    };
  }

  componentDidMount() {
    this.switchToTabAtIndex(0);
  }

  switchToTabAtIndex = index => {
    this.setState(
      {
        isRendered: this.state.isRendered.map(
          (val, currIndex) => currIndex === index || val,
        ),
        tabIndex: index,
      },
      () => {
        const {onSwitchTab} = this.props;

        if (typeof onSwitchTab === 'function') {
          onSwitchTab(index);
        }
      },
    );
  };

  onScroll = (evt = {}) => {
    const x = evt.nativeEvent.contentOffset.x;
    const screenWidth = Dimensions.get('window').width;
    const tabIndex = Math.floor(x / screenWidth);

    this.switchToTabAtIndex(tabIndex);
  };

  goToTab = index => {
    const screenWidth = Dimensions.get('window').width;
    const x = index * screenWidth;

    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo({
        x: x,
        y: 0,
        animated: true,
      });
    }
  };

  render() {
    return (
      <View style={this.props.style}>
        <ScrollView
          ref={ref => (this.scrollViewRef = ref)}
          horizontal
          style={styles.scrollView}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          bounces={false}
          decelerationRate={0}
          snapToAlignment="center"
          snapToEnd
          snapToInterval={Dimensions.get('window').width}
          onMomentumScrollEnd={this.onScroll}>
          {this.props.children.map((child, index) => {
            if (!this.state.isRendered[index]) {
              return (
                <View key={index} style={styles.dummyView}>
                  <ActivityIndicator />
                </View>
              );
            }

            return React.cloneElement(child, {
              key: index,
            });
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dummyView: {
    width: Dimensions.get('window').width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollView: {
    flex: 1,
  },
});

export default Pagination;
