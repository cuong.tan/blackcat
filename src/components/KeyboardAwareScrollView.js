import React from 'react';
import {Platform, ScrollView, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import KeyboardPaddingView from 'src/components/KeyboardPaddingView';

const KeyboardAwareScrollViewAndroid = ({children, ...props}) => {
  return (
    <View enabled behavior="height" {...props}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {children}
      </ScrollView>
      <KeyboardPaddingView />
    </View>
  );
};

export default Platform.select({
  ios: KeyboardAwareScrollView,
  android: KeyboardAwareScrollViewAndroid,
});
