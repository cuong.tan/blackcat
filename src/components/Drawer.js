import React, {Component} from 'react';
import {ScrollView, View, Dimensions, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

const INFINITY = 100000;

class Drawer extends Component {
  static propTypes = {
    renderDrawerContent: PropTypes.func,
    drawerWidth: PropTypes.number,
  };

  static defaultProps = {
    renderDrawerContent: () => {},
    drawerWidth: 300,
  };

  constructor(props) {
    super(props);

    this.isOpen = false;
  }

  toggle = () => {
    if (!this.scrollViewRef) {
      return;
    }

    this.scrollViewRef.scrollTo({
      x: this.isOpen ? INFINITY : 0,
      y: 0,
      animated: true,
    });

    this.isOpen = !this.isOpen;
  };

  render() {
    const leftPaneStyles = [
      styles.leftPane,
      {
        width: this.props.drawerWidth,
      },
    ];

    return (
      <ScrollView
        ref={ref => (this.scrollViewRef = ref)}
        horizontal
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        contentOffset={{x: INFINITY}}
        bounces={false}
        decelerationRate={0}
        snapToInterval={INFINITY}
        onScroll={e => (this.isOpen = e.nativeEvent.contentOffset.x <= 0)}
        snapToEnd
        snapToAlignment="center"
        style={{flex: 1, flexDirection: 'row'}}>
        <View style={leftPaneStyles}>{this.props.renderDrawerContent()}</View>
        <View style={styles.rightPane}>{this.props.children}</View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentWrapper: {
    flexDirection: 'row',
  },
  leftPane: {
    height: '100%',
  },
  rightPane: {
    height: '100%',
    width: Dimensions.get('window').width,
  },
});

export default Drawer;
