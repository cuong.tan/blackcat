import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {SvgXml} from 'react-native-svg';
import LinearGradient from 'react-native-linear-gradient';

import UserInteraction from 'src/components/UserInteraction';
import dateFrom from 'src/assets/svgs/date-from.svg';
import dateTo from 'src/assets/svgs/date-to.svg';
import mapImage from 'src/assets/imgs/gmap.png';
import themeColor from 'src/assets/colors/theme1';
import ChatList from 'src/components/ChatList';

const articleOriginalHeight = 187;

const InfoOutline = ({children, title = ''}) => {
  return (
    <View style={styles.infoOutline}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            backgroundColor: themeColor.primary,
            height: 24,
            width: 6,
            borderRadius: 4,
            marginRight: 6,
          }}
        />
        <Text style={{color: themeColor.primary, fontSize: 20}}>{title}</Text>
      </View>
      {children}
    </View>
  );
};

class DetailTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      articleMaxHeight: articleOriginalHeight,
    };
  }

  toggleArticleContent = () => {
    this.setState({
      articleMaxHeight: 1000 - this.state.articleMaxHeight,
    });
  };

  renderImagesGallery(images) {
    return (
      <ScrollView
        contentContainerStyle={{paddingRight: 16}}
        horizontal
        showsHorizontalScrollIndicator={false}>
        {images.map((uri, index) => (
          <Image key={index} style={styles.image} source={{uri}} />
        ))}
      </ScrollView>
    );
  }

  renderArticle(isArticleClosed, article_content) {
    return (
      <View
        style={{
          maxHeight: this.state.articleMaxHeight,
          paddingBottom: isArticleClosed ? 0 : 30,
        }}>
        <Text style={styles.articleContent}>{article_content}</Text>
        {isArticleClosed && (
          <LinearGradient
            style={styles.linearGradient}
            colors={[
              'rgba(255, 255, 255, 0)',
              'rgba(255, 255, 255, 0.7)',
              'rgba(255, 255, 255, 1)',
            ]}
            locations={[0, 0.6, 1.0]}
          />
        )}
        <TouchableOpacity
          onPress={this.toggleArticleContent}
          style={styles.viewAllTouchable}>
          <Text style={{color: '#222'}}>
            {isArticleClosed ? 'VIEW ALL' : 'CLOSE'}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderTimeIndicator(time, showHour = true) {
    return (
      <View style={styles.leftPane}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <SvgXml
            fill="#D5EF7F"
            xml={showHour ? dateFrom : dateTo}
            width={18}
            height={18}
          />
          <Text style={{marginLeft: 8, color: '#222', fontSize: 16}}>
            {time}
          </Text>
        </View>
        <Text
          style={{
            fontSize: 32,
            color: showHour ? '#AECB4F' : 'transparent',
            marginTop: 16,
          }}>
          8:30
        </Text>
      </View>
    );
  }

  renderChat = () => {
    return (
      <ChatList
        onReplyTo={this.props.onReplyTo}
        scrollEnabled={false}
        style={styles.chatList}
        conversations={this.props.itemDetails.conversations}
      />
    );
  };

  render() {
    const {
      images = [],
      article_content = '',
      start_time,
      end_time,
      place,
    } = this.props.itemDetails;
    const isArticleClosed =
      this.state.articleMaxHeight === articleOriginalHeight;

    return (
      <View>
        {this.renderImagesGallery(images)}
        {this.renderArticle(isArticleClosed, article_content)}
        <InfoOutline title="When">
          <View style={{flexDirection: 'row'}}>
            {this.renderTimeIndicator(start_time)}
            {this.renderTimeIndicator(end_time, false)}
          </View>
        </InfoOutline>
        <InfoOutline title="Where">
          <View style={{flexDirection: 'column'}}>
            <Text style={styles.placeAddress}>{place}</Text>
          </View>
          <View style={styles.mapWrapper}>
            <Image resizeMode="contain" source={mapImage} style={{flex: 1}} />
          </View>
        </InfoOutline>
        <UserInteraction itemDetails={this.props.itemDetails} />
        {this.renderChat()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  chatList: {
    margin: 16,
  },
  image: {
    height: 100,
    width: 180,
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: '#bfbfbf',
    marginLeft: 16,
    marginVertical: 12,
  },
  mapWrapper: {
    height: 88,
    flex: 1,
    marginRight: 16,
    marginTop: 16,
    backgroundColor: 'red',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    borderRadius: 12,
  },
  placeAddress: {
    fontSize: 14,
    color: '#67616D',
    marginTop: 14,
  },
  leftPane: {
    flex: 1,
    borderRightWidth: 0.5,
    height: 90,
    borderColor: '#D8D8D8',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoOutline: {
    borderTopWidth: 1,
    borderColor: '#D8D8D8',
    marginTop: 16,
    paddingVertical: 16,
    marginLeft: 16,
    alignSelf: 'stretch',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  viewAllTouchable: {
    height: 24,
    paddingHorizontal: 12,
    backgroundColor: themeColor.selection,
    borderRadius: 12,
    alignSelf: 'center',
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  linearGradient: {
    width: '100%',
    height: 57,
    position: 'absolute',
    bottom: 0,
  },
  articleContent: {
    fontSize: 14,
    color: '#67616D',
    marginHorizontal: 16,
  },
});

export default DetailTab;
