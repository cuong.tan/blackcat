import {SafeAreaView} from 'react-native-safe-area-context';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';

import search from 'src/assets/svgs/search.svg';
import themeColor from 'src/assets/colors/theme1';
import logoCat from 'src/assets/svgs/logo-cat.svg';
import user from 'src/assets/svgs/user.svg';
import {SvgXml} from 'react-native-svg';

const Button = ({xml = '', iconColor = themeColor.selection, ...rest}) => {
  return (
    <TouchableOpacity onPress={rest.onPress}>
      <View style={styles.button}>
        <SvgXml fill={iconColor} xml={xml} width={22} height={222} />
      </View>
    </TouchableOpacity>
  );
};

const Header = ({
  onLeftPress,
  leftXmlIcon = search,
  onRightPress,
  rightXmlIcon = user,
}) => {
  return (
    <SafeAreaView>
      <View style={styles.header}>
        <View style={{flex: 1}} />
        <View style={styles.headerContent}>
          <Button
            xml={leftXmlIcon}
            iconColor={themeColor.primaryDark}
            onPress={onLeftPress}
          />
          <Button xml={logoCat} />
          <Button xml={rightXmlIcon} onPress={onRightPress} />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  header: {
    width: '100%',
    backgroundColor: themeColor.primary,
    flex: 1,
  },
  headerContent: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  button: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Header;
