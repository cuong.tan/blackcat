import React, {Component} from 'react';
import {View, Dimensions} from 'react-native';
import PropTypes from 'prop-types';

class PaginationItem extends Component {
  static propTypes = {
    title: PropTypes.string,
  };

  static defaultProps = {
    title: '',
  };

  render() {
    return (
      <View
        style={[
          {
            width: Dimensions.get('window').width,
            height: '100%',
          },
          this.props.style,
        ]}>
        {this.props.children}
      </View>
    );
  }
}

export default PaginationItem;
