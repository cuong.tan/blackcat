import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {SvgXml} from 'react-native-svg';
import React, {Component} from 'react';

import search from 'src/assets/svgs/search.svg';
import themeColor from 'src/assets/colors/theme1';

class SearchButton extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[
          styles.searchButton,
          this.props.disabled ? styles.searchButtonDisabled : {},
        ]}
        onPress={this.props.disabled ? null : this.props.onPress}>
        <View style={{flexDirection: 'row'}}>
          <SvgXml
            fill={this.props.disabled ? '#666' : '#222'}
            xml={search}
            width={22}
            height={22}
          />
          <Text
            style={
              this.props.disabled
                ? styles.searchButtonTextDisabled
                : styles.searchButtonText
            }>
            SEARCH
          </Text>
        </View>
        {this.props.searchInfoText && (
          <Text style={styles.searchInfo}>{this.props.searchInfoText}</Text>
        )}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  searchButtonDisabled: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
    backgroundColor: '#BABABA',
    width: '100%',
  },
  searchButtonText: {
    fontSize: 16,
    marginLeft: 4,
    color: '#222',
  },
  searchButtonTextDisabled: {
    fontSize: 16,
    marginLeft: 4,
    color: '#666',
  },
  searchButton: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
    backgroundColor: themeColor.selection,
    width: '100%',
  },
  searchInfo: {
    fontSize: 10,
    color: themeColor.primary,
  },
});

export default SearchButton;
