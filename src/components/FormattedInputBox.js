import React, {Component} from 'react';
import {StyleSheet, TextInput} from 'react-native';
import PropTypes from 'prop-types';

import {formatWithValidDate} from 'src/utils/DateUtils';

class FormattedInputBox extends Component {
  static propTypes = {
    onChangeText: PropTypes.func,
  };

  static defaultProps = {
    onChangeText: () => {},
  };

  constructor(props) {
    super(props);

    this.state = {
      displayedValue: '',
    };
  }

  onChangeText = text => {
    const formattedValue = formatWithValidDate(text);

    this.setState(
      {
        displayedValue: formattedValue,
      },
      () => {
        this.props.onChangeText(formattedValue);
      },
    );
  };

  render() {
    return (
      <TextInput
        keyboardType="number-pad"
        style={styles.dateInput}
        placeholder="DD/MM/YYYY"
        {...this.props}
        value={this.state.displayedValue}
        onChangeText={this.onChangeText}
      />
    );
  }
}

const styles = StyleSheet.create({
  dateInput: {
    flex: 1,
    textAlign: 'center',
  },
});

export default FormattedInputBox;
