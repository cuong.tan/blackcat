import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {SvgXml} from 'react-native-svg';
import PropTypes from 'prop-types';

import themeColor from 'src/assets/colors/theme1';
import checkOutline from 'src/assets/svgs/check-outline.svg';
import likeOutline from 'src/assets/svgs/like-outline.svg';
import userIcon from 'src/assets/svgs/user.svg';

class UserInteraction extends Component {
  static propTypes = {
    itemDetails: PropTypes.object,
    multiline: PropTypes.bool,
  };

  static defaultProps = {
    itemDetails: {},
    multiline: false,
  };

  render() {
    const {like_count = 0, going_count = 0} = this.props.itemDetails;

    return (
      <View style={styles.userInteraction}>
        <View style={styles.userInteractionPart}>
          <View
            style={[
              styles.userInteractionLink,
              {
                paddingBottom: 6,
              },
            ]}>
            <SvgXml
              fill={themeColor.primary}
              xml={checkOutline}
              width="14"
              height="14"
            />
            <Text style={styles.userInteractionLinkText}>
              {going_count} going
            </Text>
            {this.renderUserAvatars(going_count)}
          </View>
        </View>
        <View
          style={[
            styles.userInteractionPart,
            {borderTopWidth: 1, borderColor: '#D8D8D8', paddingTop: 6},
          ]}>
          <View style={styles.userInteractionLink}>
            <SvgXml
              fill={themeColor.primary}
              xml={likeOutline}
              width="14"
              height="14"
            />
            <Text style={styles.userInteractionLinkText}>
              {like_count} likes
            </Text>
          </View>
          {this.renderUserAvatars(like_count)}
        </View>
      </View>
    );
  }

  renderUserAvatars = (count = 10) => {
    let avatars = [];

    for (let i = 0; i < count; i++) {
      avatars.push(
        <SvgXml
          key={i}
          style={{marginRight: 8}}
          fill="#B2B2B2"
          width="26"
          height="26"
          xml={userIcon}
        />,
      );
    }

    if (this.props.multiline) {
      return (
        <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
          {avatars}
        </View>
      );
    }

    return (
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <View style={{flex: 1, flexDirection: 'row'}}>{avatars}</View>
      </ScrollView>
    );
  };
}

const styles = StyleSheet.create({
  userInteractionLinkText: {
    marginLeft: 8,
    fontSize: 14,
    color: '#222',
    width: 80,
  },
  userInteractionLink: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  userInteraction: {
    paddingVertical: 8,
    width: '100%',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#D8D8D8',
    paddingLeft: 0,
  },
  userInteractionPart: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginLeft: 16,
  },
});

export default UserInteraction;
