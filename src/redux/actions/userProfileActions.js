import {createAsyncAction} from 'src/utils/reduxUtils';
import {itemDetailsActions} from 'src/redux/actions/itemDetailsActions';

const userProfileActionTypes = {
  RETRIEVE_USER_PROFILE: 'RETRIEVE_USER_PROFILE',
};

const retrieveUserProfile = ({user_token = ''}) => {
  return createAsyncAction({
    api: '/user_profile',
    type: userProfileActionTypes.RETRIEVE_USER_PROFILE,
    method: 'GET',
    params: {
      user_token,
    },
  });
};

export {retrieveUserProfile, userProfileActionTypes};
