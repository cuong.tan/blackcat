import {createAsyncAction} from '../../utils/reduxUtils';

const actionTypes = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
};

const login = ({user_id = '', password = '', onSuccess}) =>
  createAsyncAction({
    api: 'login',
    type: actionTypes.LOGIN,
    params: {
      user_id,
      password,
    },
    onSuccess: onSuccess,
  });

const logout = () => dispatch =>
  dispatch({
    type: actionTypes.LOGOUT,
    payload: {},
  });

export {login, logout, actionTypes};
