import {createAsyncAction} from 'src/utils/reduxUtils';

const itemDetailsActions = {
  RETRIEVE_ITEM_DETAILS: 'RETRIEVE_ITEM_DETAILS',
  ADD_COMMENT: 'ADD_COMMENT',
  LIKE_ACTIVITY: 'LIKE_ACTIVITY',
  UNLIKE_ACTIVITY: 'UNLIKE_ACTIVITY',
  GOING_ACTIVITY: 'GOING_ACTIVITY',
  UNGOING_ACTIVITY: 'UNGOING_ACTIVITY',
};

const addComment = ({
  comment = '',
  user_token = '',
  activity_id = 0,
  onSuccess,
}) => {
  return createAsyncAction({
    api: '/add_comment',
    type: itemDetailsActions.ADD_COMMENT,
    method: 'POST',
    body: JSON.stringify({
      comment,
      user_token,
      activity_id,
    }),
    onSuccess: onSuccess,
  });
};

const retrieveItemDetails = ({item_id = ''}) => {
  return createAsyncAction({
    api: '/get_details',
    type: itemDetailsActions.RETRIEVE_ITEM_DETAILS,
    params: {
      item_id,
    },
  });
};

const likeActivity = ({user_token = '', activity_id = ''}) => {
  return createAsyncAction({
    api: '/like_activity',
    type: itemDetailsActions.LIKE_ACTIVITY,
    method: 'GET',
    params: {
      user_token,
      activity_id,
    },
  });
};

const unlikeActivity = ({user_token = '', activity_id = ''}) => {
  return createAsyncAction({
    api: '/unlike_activity',
    type: itemDetailsActions.UNLIKE_ACTIVITY,
    method: 'GET',
    params: {
      user_token,
      activity_id,
    },
  });
};

const goingActivity = ({user_token = '', activity_id = ''}) => {
  return createAsyncAction({
    api: '/going_to_activity',
    type: itemDetailsActions.GOING_ACTIVITY,
    method: 'GET',
    params: {
      user_token,
      activity_id,
    },
  });
};

const ungoingActivity = ({user_token = '', activity_id = ''}) => {
  return createAsyncAction({
    api: '/ungoing_to_activity',
    type: itemDetailsActions.UNGOING_ACTIVITY,
    method: 'GET',
    params: {
      user_token,
      activity_id,
    },
  });
};

export {
  itemDetailsActions,
  retrieveItemDetails,
  addComment,
  likeActivity,
  unlikeActivity,
  goingActivity,
  ungoingActivity,
};
