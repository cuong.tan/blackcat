import {createAsyncAction} from 'src/utils/reduxUtils';

const listingActions = {
  RETRIEVE_LIST: 'RETRIEVE_LIST',
  TOGGLE_GOING: 'TOGGLE_GOING',
  RETRIEVE_FILTER: 'RETRIEVE_FILTER',
  SEARCH_ITEMS: 'SEARCH_ITEMS',
  CLEAR_SEARCH: 'CLEAR_SEARCH',
};

const retrieveList = ({user_token = '', page_index = 0}) => {
  return createAsyncAction({
    api: 'list',
    type: listingActions.RETRIEVE_LIST,
    method: 'GET',
    params: {
      user_token,
      page_index,
    },
  });
};

const toggleGoing = ({user_token = '', item_id = ''} = {}) => {
  return createAsyncAction({
    api: 'toggle_going',
    type: listingActions.TOGGLE_GOING,
    method: 'POST',
    body: JSON.stringify({
      user_token,
      item_id,
    }),
  });
};

const retrieveFilters = () => {
  return createAsyncAction({
    api: '/filter/list',
    type: listingActions.RETRIEVE_FILTER,
    method: 'GET',
  });
};

const searchItems = ({
  user_token = '',
  date_filter = '',
  channel_filter = '',
  from_date = '',
  to_date = '',
}) => {
  return createAsyncAction({
    api: '/search',
    type: listingActions.SEARCH_ITEMS,
    method: 'GET',
    params: {
      user_token,
      date_filter,
      channel_filter,
      from_date,
      to_date,
    },
  });
};

const clearSearch = () => dispatch => {
  dispatch({
    type: listingActions.CLEAR_SEARCH,
    payload: {},
  });
};

export {
  retrieveList,
  toggleGoing,
  retrieveFilters,
  searchItems,
  clearSearch,
  listingActions,
};
