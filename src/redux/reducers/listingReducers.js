import {toRequest, toSuccess, toFailure} from 'src/utils/reduxUtils';
import {listingActions} from 'src/redux/actions/listingActions';

const initialState = {
  list: {
    loading: false,
    data: [],
  },
  filters: {
    loading: false,
    by_date: [],
    by_channel: [],
  },
  search: {
    loading: false,
    data: [],
    found: true,
    page_index: 0,
  },
};

const listingReducers = (state = initialState, action = {}) => {
  switch (action.type) {
    case listingActions.CLEAR_SEARCH:
      return {
        ...state,
        search: {
          ...initialState.search,
        },
      };
    case toRequest(listingActions.SEARCH_ITEMS):
      return {
        ...state,
        search: {
          ...state.data,
          loading: true,
          data: [],
          found: true,
        },
      };
    case toFailure(listingActions.SEARCH_ITEMS):
      return {
        ...state,
        search: {
          ...initialState.search,
        },
      };
    case toSuccess(listingActions.SEARCH_ITEMS):
      return {
        ...state,
        search: {
          loading: false,
          data: [...action.payload.list],
          found: action.payload.list.length > 0,
        },
      };
    case toRequest(listingActions.RETRIEVE_LIST):
      return {
        ...state,
        list: {
          ...state.list,
          loading: true,
        },
      };
    case toSuccess(listingActions.RETRIEVE_LIST): {
      let newData = [...state.list.data];

      if (action.payload.page_index !== 0) {
        newData = newData.concat(action.payload.list);
      } else {
        newData = [...action.payload.list];
      }

      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          data: newData,
          page_index: action.payload.page_index,
        },
      };
    }
    case toFailure(listingActions.RETRIEVE_LIST):
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
        },
      };
    case toSuccess(listingActions.TOGGLE_GOING): {
      const index = state.list.data.findIndex(
        item => item.id === action.payload.item_id,
      );

      if (index === -1) {
        return state;
      }

      const isGoing = state.list.data[index].is_going;
      const newList = [...state.list.data];
      newList[index].is_going = !isGoing;

      return {
        ...state,
        list: {
          ...state.list,
          data: newList,
        },
      };
    }
    case toSuccess(listingActions.RETRIEVE_FILTER):
      return {
        ...state,
        filters: {
          ...state.filters,
          loading: false,
          by_date: [...action.payload.by_date],
          by_channel: [...action.payload.by_channel],
        },
      };
    case toRequest(listingActions.RETRIEVE_FILTER):
      return {
        ...state,
        filters: {
          ...state.filters,
          loading: true,
          by_date: [],
          by_channel: [],
        },
      };
    case toFailure(listingActions.RETRIEVE_FILTER):
      return {
        ...state,
        filters: {
          ...state.filters,
          loading: false,
        },
      };
    default:
      return state;
  }
};

export default listingReducers;
