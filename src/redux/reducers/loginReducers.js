import {toRequest, toSuccess, toFailure} from '../../utils/reduxUtils';
import {actionTypes} from '../actions/loginActions';

const initialState = {
  userInfo: {
    user_id: '',
    name: '',
    token: undefined,
    email: '',
  },
  isLoggingIn: false,
};

const loginReducers = (state = initialState, action = {}) => {
  switch (action.type) {
    case toRequest(actionTypes.LOGIN):
      return {
        ...state,
        isLoggingIn: true,
      };
    case toSuccess(actionTypes.LOGIN):
      return {
        ...state,
        userInfo: {
          ...state.userInfo,
          ...(action.payload || {}),
        },
        isLoggingIn: false,
      };
    case toFailure(actionTypes.LOGIN):
      return {
        ...state,
        userInfo: {
          ...initialState.userInfo,
        },
        isLoggingIn: false,
      };
    case actionTypes.LOGOUT:
      return {
        ...state,
        userInfo: {
          ...initialState.userInfo,
        },
      };
    default:
      return state;
  }
};

export default loginReducers;
