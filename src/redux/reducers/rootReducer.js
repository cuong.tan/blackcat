import {combineReducers} from 'redux';

import loginReducers from 'src/redux/reducers/loginReducers';
import listingReducers from 'src/redux/reducers/listingReducers';
import itemDetailsReducers from 'src/redux/reducers/itemDetailsReducers';
import userProfileReducers from 'src/redux/reducers/userProfileReducers';

const rootReducer = combineReducers({
  login: loginReducers,
  listing: listingReducers,
  item_details: itemDetailsReducers,
  user_profile: userProfileReducers,
});

export default rootReducer;
