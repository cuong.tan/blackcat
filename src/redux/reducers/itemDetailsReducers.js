import {itemDetailsActions} from 'src/redux/actions/itemDetailsActions';
import {toFailure, toRequest, toSuccess} from 'src/utils/reduxUtils';

const initialState = {
  details: {
    loading: false,
    isAddingComment: false,
    data: {},
  },
};

const itemDetailsReducers = (state = initialState, action = {}) => {
  switch (action.type) {
    case toRequest(itemDetailsActions.RETRIEVE_ITEM_DETAILS):
      return {
        ...state,
        details: {
          ...state.details,
          loading: true,
        },
      };
    case toSuccess(itemDetailsActions.RETRIEVE_ITEM_DETAILS):
      return {
        ...state,
        details: {
          ...state.details,
          data: {
            ...action.payload,
          },
          loading: false,
        },
      };
    case toFailure(itemDetailsActions.RETRIEVE_ITEM_DETAILS):
      return {
        ...state,
        details: {
          ...state.details,
          data: {},
          loading: false,
        },
      };
    case toRequest(itemDetailsActions.ADD_COMMENT):
      return {
        ...state,
        details: {
          ...state.details,
          isAddingComment: true,
        },
      };
    case toFailure(itemDetailsActions.ADD_COMMENT):
      return {
        ...state,
        details: {
          ...state.details,
          isAddingComment: false,
        },
      };
    case toSuccess(itemDetailsActions.ADD_COMMENT):
      return {
        ...state,
        details: {
          ...state.details,
          isAddingComment: false,
          data: {
            ...state.details.data,
            conversations: [
              {
                ...action.payload,
              },
              ...state.details.data.conversations,
            ],
          },
        },
      };
    case toSuccess(itemDetailsActions.LIKE_ACTIVITY):
      return {
        ...state,
        details: {
          ...state.details,
          data: {
            ...state.details.data,
            is_like: true,
          },
        },
      };
    case toSuccess(itemDetailsActions.UNLIKE_ACTIVITY):
      return {
        ...state,
        details: {
          ...state.details,
          data: {
            ...state.details.data,
            is_like: false,
          },
        },
      };
    case toSuccess(itemDetailsActions.GOING_ACTIVITY):
      return {
        ...state,
        details: {
          ...state.details,
          data: {
            ...state.details.data,
            is_going: true,
          },
        },
      };
    case toSuccess(itemDetailsActions.UNGOING_ACTIVITY):
      return {
        ...state,
        details: {
          ...state.details,
          data: {
            ...state.details.data,
            is_going: false,
          },
        },
      };
    default:
      return state;
  }
};

export default itemDetailsReducers;
