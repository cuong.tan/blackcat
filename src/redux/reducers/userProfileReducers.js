import {userProfileActionTypes} from 'src/redux/actions/userProfileActions';
import {toFailure, toRequest, toSuccess} from 'src/utils/reduxUtils';

const initialState = {
  profile: {
    data: {},
    loading: false,
  },
};

const userProfileReducers = (state = initialState, action = {}) => {
  switch (action.type) {
    case toRequest(userProfileActionTypes.RETRIEVE_USER_PROFILE):
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: true,
        },
      };
    case toSuccess(userProfileActionTypes.RETRIEVE_USER_PROFILE):
      return {
        ...state,
        profile: {
          ...state.profile,
          data: {
            ...action.payload,
          },
          loading: false,
        },
      };
    case toFailure(userProfileActionTypes.RETRIEVE_USER_PROFILE):
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: false,
        },
      };
    default:
      return state;
  }
};

export default userProfileReducers;
