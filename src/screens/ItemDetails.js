import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {connect} from 'react-redux';
import {SvgXml} from 'react-native-svg';

import Header from 'src/components/Header';
import themeColor from 'src/assets/colors/theme1';
import {
  retrieveItemDetails,
  addComment,
  likeActivity,
  unlikeActivity,
  goingActivity,
  ungoingActivity,
} from 'src/redux/actions/itemDetailsActions';
import userIcon from 'src/assets/svgs/user.svg';
import commentSingleIcon from 'src/assets/svgs/comment-single.svg';
import likeOutlineIcon from 'src/assets/svgs/like-outline.svg';
import likeIcon from 'src/assets/svgs/like.svg';
import crossIcon from 'src/assets/svgs/cross.svg';
import sendIcon from 'src/assets/svgs/send.svg';
import homeIcon from 'src/assets/svgs/home.svg';
import checkOutlineIcon from 'src/assets/svgs/check-outline.svg';
import checkIcon from 'src/assets/svgs/check.svg';
import Pagination from 'src/components/Pagination';
import PaginationItem from 'src/components/PaginationItem';
import DetailTab from 'src/components/DetailTab';
import ChatList from 'src/components/ChatList';
import PaginationHeader from 'src/components/PaginationHeader';
import UserInteraction from 'src/components/UserInteraction';
import KeyboardPaddingView from 'src/components/KeyboardPaddingView';
import screenIds from 'src/assets/constants/screens';

class ItemDetails extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentTabIndex: 0,
      messageContent: '',
      repliedComment: {},
    };
  }

  componentDidMount() {
    const item_id = this.props.navigation.getParam('item_id', '');

    this.props.retrieveItemDetails({item_id});
  }

  reply = comment => {
    this.setState(
      {
        repliedComment: comment,
        currentTabIndex: 2,
      },
      () => {
        if (this.pageRef && typeof this.pageRef.goToTab === 'function') {
          this.pageRef.goToTab(2);
        }

        if (this.textInputRef) {
          this.textInputRef.focus();
        }
      },
    );
  };

  onSwitchTab = tabIndex => {
    this.setState(
      {
        currentTabIndex: tabIndex,
      },
      () => {
        if (this.scrollViewRef) {
          this.scrollViewRef.scrollTo({x: 0, y: 0, animated: true});
        }
      },
    );
  };

  dismissMessage = () => {
    this.setState({messageContent: '', repliedComment: {}});
  };

  sendMessage = () => {
    const {full_name = ''} = this.state.repliedComment;

    if (this.state.messageContent.trim().length === 0) {
      this.dismissMessage();
      return;
    }

    this.props.addComment({
      user_token: this.props.userInfo.token,
      comment:
        (full_name.length > 0 ? `@${full_name}: ` : '') +
        this.state.messageContent,
      onSuccess: this.dismissMessage,
    });
  };

  showComments = () => {
    this.setState(
      {
        currentTabIndex: 2,
      },
      () => {
        if (this.textInputRef) {
          this.textInputRef.focus();
        }

        if (this.pageRef && typeof this.pageRef.goToTab === 'function') {
          this.pageRef.goToTab(2);
        }
      },
    );
  };

  toggleLike = () => {
    const {itemDetails, likeActivity, unlikeActivity, userInfo} = this.props;

    if (itemDetails.is_like) {
      unlikeActivity({
        user_token: userInfo.token,
        activity_id: itemDetails.id,
      });
    } else {
      likeActivity({
        user_token: userInfo.token,
        activity_id: itemDetails.id,
      });
    }
  };

  toggleGoing = () => {
    const {itemDetails, goingActivity, ungoingActivity, userInfo} = this.props;

    if (itemDetails.is_going) {
      ungoingActivity({
        user_token: userInfo.token,
        activity_id: itemDetails.id,
      });
    } else {
      goingActivity({
        user_token: userInfo.token,
        activity_id: itemDetails.id,
      });
    }
  };

  renderWaiting = () => {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator
          color={themeColor.primaryDark}
          style={{marginBottom: 4}}
        />
        <Text style={{color: themeColor.primaryDark}}>Loading...</Text>
      </View>
    );
  };

  renderCommentsTab = (itemDetails = {}) => {
    return (
      <ChatList
        style={{flex: 1, padding: 16}}
        conversations={itemDetails.conversations}
        scrollEnabled={false}
        onReplyTo={this.reply}
      />
    );
  };

  renderArticleInfo = ({
    channel_id = '',
    title = '',
    user = {},
    published_date = '',
  } = {}) => {
    return (
      <>
        <View style={styles.channel}>
          <Text style={styles.channelText}>{channel_id}</Text>
        </View>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.userInfo}>
          <SvgXml xml={userIcon} width={64} height={64} fill="#ccc" />
          <View style={{marginLeft: 8}}>
            <Text style={{fontSize: 14, color: '#222'}}>{user.full_name}</Text>
            <Text style={{fontSize: 12, color: '#ccc', marginTop: 4}}>
              {published_date}
            </Text>
          </View>
        </View>
      </>
    );
  };

  renderChatBox = () => {
    const {full_name = ''} = this.state.repliedComment;

    return (
      <View style={styles.chatBox}>
        <TouchableOpacity
          style={styles.crossTouchable}
          onPress={this.dismissMessage}>
          <SvgXml
            xml={crossIcon}
            width={17}
            height={17}
            fill={themeColor.selection}
          />
        </TouchableOpacity>
        <View style={styles.chatInputWrapper} onPress={this.props.likeActivity}>
          <TextInput
            ref={ref => (this.textInputRef = ref)}
            placeholder={
              full_name.length > 0 ? `@${full_name}` : 'Leave your comment here'
            }
            onSubmitEditing={this.sendMessage}
            editable={!this.props.isAddingComment}
            style={styles.chatInput}
            value={this.state.messageContent}
            onChangeText={messageContent => this.setState({messageContent})}
          />
        </View>
        {this.props.isAddingComment ? (
          <ActivityIndicator style={styles.sendTouchable} />
        ) : (
          <TouchableOpacity
            style={styles.sendTouchable}
            onPress={this.sendMessage}>
            <SvgXml
              xml={sendIcon}
              width={28}
              height={24}
              fill={themeColor.primary}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  renderActivityButtons = () => {
    return (
      <View style={styles.chatBox}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: themeColor.primary,
          }}>
          <TouchableOpacity
            style={styles.activityButton}
            onPress={this.showComments}>
            <SvgXml
              xml={commentSingleIcon}
              width={24}
              height={24}
              fill={themeColor.primaryDark}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.activityButton}
            onPress={this.toggleLike}>
            <SvgXml
              xml={this.props.itemDetails.is_like ? likeIcon : likeOutlineIcon}
              width={24}
              height={24}
              fill={themeColor.primaryDark}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.joinTouchable}
          onPress={this.toggleGoing}>
          <SvgXml
            xml={this.props.itemDetails.is_going ? checkIcon : checkOutlineIcon}
            width={24}
            height={24}
            fill="#788C36"
          />
          <Text style={{marginLeft: 8}}>
            {this.props.itemDetails.is_going ? 'I am going' : 'Join'}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderIcon = ({tabIndex, selected}) => {};

  renderButtons = () => {
    return (
      <View style={styles.bottomButtons}>
        {this.state.currentTabIndex === 2
          ? this.renderChatBox()
          : this.renderActivityButtons()}
        <KeyboardPaddingView />
      </View>
    );
  };

  renderParticipantsTab = () => {
    return (
      <View style={{flex: 1}}>
        <UserInteraction multiline itemDetails={this.props.itemDetails} />
        {this.renderCommentsTab(this.props.itemDetails)}
      </View>
    );
  };

  renderContent = (itemDetails = {}) => {
    return (
      <>
        <ScrollView
          ref={ref => (this.scrollViewRef = ref)}
          contentInset={{bottom: 56}}
          style={styles.scrollView}
          stickyHeaderIndices={[1]}>
          {this.renderArticleInfo(itemDetails)}
          <PaginationHeader
            titles={['Details', 'Participants', 'Comments']}
            tabIndex={this.state.currentTabIndex}
            goToTab={index => {
              if (this.pageRef && typeof this.pageRef.goToTab === 'function') {
                this.pageRef.goToTab(index);
              }

              this.setState({
                currentTabIndex: index,
              });
            }}
          />
          <Pagination
            ref={ref => (this.pageRef = ref)}
            onSwitchTab={this.onSwitchTab}>
            <PaginationItem
              title="Details"
              selectedIcon={this.renderIcon({tabIndex: 0, selected: true})}
              icon={this.renderIcon({tabIndex: 0, selected: false})}>
              <DetailTab itemDetails={itemDetails} onReplyTo={this.reply} />
            </PaginationItem>
            <PaginationItem title="Participants">
              {this.renderParticipantsTab()}
            </PaginationItem>
            <PaginationItem title="Comments">
              {this.renderCommentsTab(itemDetails)}
            </PaginationItem>
          </Pagination>
        </ScrollView>
        {this.renderButtons()}
      </>
    );
  };

  render() {
    return (
      <View style={styles.itemDetails}>
        <Header
          leftXmlIcon={homeIcon}
          onLeftPress={() => this.props.navigation.goBack()}
          onRightPress={() =>
            this.props.navigation.navigate(screenIds.USER_PROFILE)
          }
        />
        {this.props.isLoadingDetails
          ? this.renderWaiting()
          : this.renderContent(this.props.itemDetails || {})}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  activityButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  joinTouchable: {
    height: '100%',
    width: 140,
    backgroundColor: themeColor.selection,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  chatInput: {
    fontSize: 14,
    color: '#222',
  },
  chatInputWrapper: {
    flex: 1,
    height: 32,
    borderRadius: 16,
    backgroundColor: '#fff',
    marginRight: 16,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  crossTouchable: {
    height: 36,
    width: 48,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sendTouchable: {
    width: 64,
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: themeColor.selection,
  },
  chatBox: {
    flex: 1,
    backgroundColor: themeColor.primary,
    minHeight: 56,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomButtons: {
    flexDirection: 'column',
    width: '100%',
    minHeight: 56,
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: themeColor.primary,
  },
  userInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 16,
    marginHorizontal: 16,
    marginBottom: 8,
  },
  title: {
    fontSize: 26,
    color: themeColor.primaryDark,
    marginTop: 16,
    marginHorizontal: 16,
  },
  channel: {
    marginTop: 16,
    marginHorizontal: 16,
    paddingHorizontal: 8,
    height: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: themeColor.primary,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-start',
  },
  channelText: {
    fontSize: 12,
    color: themeColor.primary,
  },
  itemDetails: {
    flex: 1,
    backgroundColor: themeColor.primary,
  },
  scrollView: {
    width: '100%',
    backgroundColor: '#fff',
  },
});

const mapStateToProps = state => ({
  userInfo: state.login.userInfo,
  itemDetails: state.item_details.details.data,
  isLoadingDetails: state.item_details.details.loading,
  isAddingComment: state.item_details.details.isAddingComment,
});

export default connect(
  mapStateToProps,
  {
    retrieveItemDetails,
    addComment,
    likeActivity,
    unlikeActivity,
    goingActivity,
    ungoingActivity,
  },
)(ItemDetails);
