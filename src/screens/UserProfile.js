import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, Text} from 'react-native';
import {connect} from 'react-redux';

import homeIcon from 'src/assets/svgs/home.svg';
import Header from 'src/components/Header';
import themeColor from 'src/assets/colors/theme1';
import {retrieveUserProfile} from 'src/redux/actions/userProfileActions';
import {logout} from 'src/redux/actions/loginActions';
import {SvgXml} from 'react-native-svg';
import userIcon from 'src/assets/svgs/user.svg';
import dateToIcon from 'src/assets/svgs/date-to.svg';
import emailIcon from 'src/assets/svgs/email.svg';
import PaginationHeader from 'src/components/PaginationHeader';
import Pagination from 'src/components/Pagination';
import PaginationItem from 'src/components/PaginationItem';
import ActivityList from 'src/components/ActivityList';
import ActivityListItem from 'src/components/ActivityListItem';

class UserProfile extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentTabIndex: 0,
    };
  }

  componentDidMount() {
    this.props.retrieveUserProfile({user_token: this.props.userInfo.token});
  }

  onSwitchTab = tabIndex => {
    this.setState({
      currentTabIndex: tabIndex,
    });
  };

  goToTab = index => {
    if (this.pageRef && typeof this.pageRef.goToTab === 'function') {
      this.pageRef.goToTab(index);
    }

    this.setState({
      currentTabIndex: index,
    });
  };

  renderUserInfo = () => {
    const {name, email} = this.props.userInfo;

    return (
      <View style={styles.userInfoContainer}>
        <SvgXml
          fill={themeColor.primary}
          xml={userIcon}
          width={72}
          height={72}
        />
        <Text style={styles.fullName}>{name}</Text>
        <View style={styles.emailContainer}>
          <SvgXml
            fill={themeColor.primary}
            xml={emailIcon}
            width={16}
            height={16}
          />
          <Text style={styles.email}>{email}</Text>
        </View>
      </View>
    );
  };

  renderActivityListItem = ({item = {}, index}) => {
    return <ActivityListItem key={index} data={item} />;
  };

  render() {
    const {likes = [], goings = [], pasts = []} = this.props.userProfile;
    return (
      <View style={styles.userProfile}>
        <Header
          leftXmlIcon={homeIcon}
          onLeftPress={() => this.props.navigation.goBack()}
          rightXmlIcon={dateToIcon}
          onRightPress={this.props.logout}
        />
        <ScrollView style={styles.content} stickyHeaderIndices={[1]}>
          {this.renderUserInfo()}
          <PaginationHeader
            titles={[
              `${likes.length} Likes`,
              `${goings.length} Going`,
              `${pasts.length} Past`,
            ]}
            tabIndex={this.state.currentTabIndex}
            goToTab={this.goToTab}
          />
          <Pagination
            ref={ref => (this.pageRef = ref)}
            onSwitchTab={this.onSwitchTab}>
            <PaginationItem>
              <ActivityList
                activities={likes}
                renderItem={this.renderActivityListItem}
                scrollEnabled={false}
              />
            </PaginationItem>
            <PaginationItem>
              <ActivityList
                activities={goings}
                renderItem={this.renderActivityListItem}
                scrollEnabled={false}
              />
            </PaginationItem>
            <PaginationItem>
              <ActivityList
                activities={pasts}
                renderItem={this.renderActivityListItem}
                scrollEnabled={false}
              />
            </PaginationItem>
          </Pagination>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fullName: {
    fontSize: 24,
    color: '#67616D',
    marginTop: 20,
    marginBottom: 8,
  },
  emailContainer: {
    flexDirection: 'row',
  },
  email: {
    fontSize: 14,
    color: themeColor.primary,
    marginLeft: 8,
  },
  userInfoContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 32,
  },
  userProfile: {
    flex: 1,
    backgroundColor: themeColor.primary,
  },
  content: {
    width: '100%',
    backgroundColor: '#fff',
  },
});

const mapStateToProps = state => ({
  userInfo: state.login.userInfo,
  userProfile: state.user_profile.profile.data,
});

export default connect(
  mapStateToProps,
  {retrieveUserProfile, logout},
)(UserProfile);
