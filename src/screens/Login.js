import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {SvgXml} from 'react-native-svg';
import {connect} from 'react-redux';

import backgroundImage from 'src/assets/imgs/Street-Dance-01.jpg';
import logoCat from 'src/assets/svgs/logo-cat.svg';
import userIcon from 'src/assets/svgs/user.svg';
import passwordIcon from 'src/assets/svgs/password.svg';
import themeColor from 'src/assets/colors/theme1';
import {login} from 'src/redux/actions/loginActions';
import screenIds from '../assets/constants/screens';
import KeyboardPaddingView from 'src/components/KeyboardPaddingView';
import KeyboardAwareScrollView from 'src/components/KeyboardAwareScrollView';

class Login extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      userToken: undefined,
    };
  }

  componentDidUpdate() {
    this.showListingIfLoggedIn();
  }

  componentDidMount() {
    this.showListingIfLoggedIn();
  }

  showListingIfLoggedIn = () => {
    if (this.props.userInfo.token !== undefined) {
      this.props.navigation.navigate(screenIds.LISTING_NAV);
    }
  };

  renderAppLogoContainer = () => {
    return (
      <View style={styles.appLogoContainer}>
        <Text
          style={{
            fontSize: 14,
            color: themeColor.selection,
            fontFamily: 'Helvetica',
          }}>
          FIND THE MOST LOVED ACTIVITIES
        </Text>
        <Text
          style={{
            fontSize: 24,
            color: themeColor.selection,
            fontFamily: 'Helvetica',
            paddingTop: 16,
          }}>
          BLACK CAT
        </Text>
        <View style={styles.outerCircle}>
          <View style={styles.innerCircle}>
            <SvgXml
              fill={themeColor.selection}
              xml={logoCat}
              width="37"
              height="43"
            />
          </View>
        </View>
      </View>
    );
  };

  renderInputBox = ({placeholder = '', xmlIcon, ...rest} = {}) => {
    return (
      <View style={styles.inputContainer}>
        <SvgXml
          fill="#fff"
          fillOpacity={0.3}
          xml={xmlIcon}
          width="18"
          height="18"
        />
        <TextInput
          placeholder={placeholder}
          style={styles.textInput}
          clearButtonMode="while-editing"
          selectionColor={themeColor.selection}
          autoCapitalize="none"
          {...rest}
        />
      </View>
    );
  };

  renderLoginForm = () => {
    const {isLoggingIn} = this.props;

    return (
      <View style={styles.loginForm}>
        {this.renderInputBox({
          placeholder: 'Email',
          xmlIcon: userIcon,
          autoCompleteType: 'email',
          editable: !isLoggingIn,
          onSubmitEditing: () => this.passwordRef && this.passwordRef.focus(),
          onChangeText: email => this.setState({email}),
        })}
        {this.renderInputBox({
          ref: ref => (this.passwordRef = ref),
          placeholder: 'Password',
          xmlIcon: passwordIcon,
          autoCompleteType: 'password',
          secureTextEntry: true,
          editable: !isLoggingIn,
          onSubmitEditing: this.proceedLogin,
          onChangeText: password => this.setState({password}),
        })}
      </View>
    );
  };

  proceedLogin = () => {
    const {navigation} = this.props;
    const {email, password} = this.state;

    this.props.login({
      user_id: email,
      password: password,
      onSuccess: () => navigation.navigate(screenIds.LISTING_NAV),
    });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          resizeMode="cover"
          style={styles.background}
          source={backgroundImage}>
          <LinearGradient
            colors={['rgba(133, 96, 169, 0.7)', 'rgba(133, 96, 169, 1.0)']}
            style={StyleSheet.absoluteFill}>
            <KeyboardAwareScrollView style={{flex: 1}}>
              <View
                style={{
                  width: '100%',
                  height: Dimensions.get('window').height,
                }}>
                <SafeAreaView style={styles.safeArea}>
                  <View style={styles.upperForm}>
                    {this.renderAppLogoContainer()}
                  </View>
                  <View style={styles.lowerForm}>{this.renderLoginForm()}</View>
                </SafeAreaView>
                <TouchableOpacity
                  style={styles.signInButton}
                  onPress={this.proceedLogin}>
                  {this.props.isLoggingIn && <ActivityIndicator />}
                  <Text style={styles.signInButtonText}>SIGN IN</Text>
                </TouchableOpacity>
                <KeyboardPaddingView />
              </View>
            </KeyboardAwareScrollView>
          </LinearGradient>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.login.userInfo,
  isLoggingIn: state.login.isLoggingIn,
});

export default connect(
  mapStateToProps,
  {
    login: login,
  },
)(Login);

const styles = StyleSheet.create({
  upperForm: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerForm: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  signInButton: {
    width: '100%',
    height: 64,
    backgroundColor: themeColor.selection,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  signInButtonText: {
    fontSize: 16,
  },
  appLogoContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputContainer: {
    backgroundColor: 'rgba(255, 255, 255, 0.20)',
    borderWidth: 1,
    borderColor: '#D3C1E5',
    borderRadius: 20,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 14,
    alignSelf: 'stretch',
  },
  textInput: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    marginLeft: 10,
    flex: 1,
    color: '#453257',
  },
  safeArea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  background: {
    ...StyleSheet.absoluteFillObject,
  },
  loginForm: {
    width: 240,
    height: 100,
    justifyContent: 'space-between',
    flexDirection: 'column',
    alignItems: 'center',
  },
  outerCircle: {
    height: 64,
    width: 64,
    borderWidth: 1,
    borderColor: 'rgba(213, 239, 127, 0.3)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    marginTop: 26,
  },
  innerCircle: {
    height: 60,
    width: 60,
    borderWidth: 1,
    borderColor: themeColor.selection,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
});
