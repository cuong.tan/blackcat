import React, {Component} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {SvgXml} from 'react-native-svg';

import Drawer from 'src/components/Drawer';
import themeColor from 'src/assets/colors/theme1';
import {logout} from 'src/redux/actions/loginActions';
import screenIds from 'src/assets/constants/screens';
import noActivity from 'src/assets/svgs/no-activity.svg';
import {
  clearSearch,
  retrieveFilters,
  retrieveList,
  searchItems,
  toggleGoing,
} from 'src/redux/actions/listingActions';
import Filter from 'src/components/Filter';
import Header from 'src/components/Header';
import {isValidDate} from 'src/utils/DateUtils';
import {showMessage} from 'src/utils/showMessage';
import ActivityList from 'src/components/ActivityList';
import ActivityListItem from 'src/components/ActivityListItem';

class Listing extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  static defaultProps = {
    userInfo: {},
    list: [],
    filterByDate: {},
    filterByChannel: {},
    searchResults: [],
    searchFound: true,
    isSearching: false,
    page_index: 0,
  };

  constructor(props) {
    super(props);

    this.filterDate = {};
    this.filterChannel = {};
  }

  componentDidUpdate() {
    const {userInfo = {}} = this.props;

    if (userInfo.token === undefined) {
      this.props.navigation.navigate(screenIds.LOGIN_NAV);
    }
  }

  componentDidMount() {
    const {userInfo} = this.props;

    this.props.retrieveFilters();

    if (userInfo.token) {
      this.props.retrieveList({user_token: userInfo.token});
    }
  }

  loadNextPage = () => {
    const {userInfo, isLoadingPage} = this.props;

    if (userInfo.token && !isLoadingPage) {
      this.props.retrieveList({
        user_token: userInfo.token,
        page_index: this.props.pageIndex + 1,
      });
    }
  };

  proceedToggleGoing = item_id => () => {
    const {userInfo = {}} = this.props;

    this.props.toggleGoing({
      user_token: userInfo.token || '',
      item_id: item_id,
    });
  };

  filter = ({dateFilter, channelFilter, dateRange = {}} = {}) => {
    if (
      dateFilter.label === 'LATER' &&
      !isValidDate(dateRange.fromDate) &&
      !isValidDate(dateRange.toDate)
    ) {
      showMessage('Please input valid dates', 'Error');
      return;
    }

    this.props.searchItems({
      date_filter: dateFilter.label || 'ANYTIME',
      channel_filter: channelFilter.label || '',
      user_token: this.props.userInfo.token,
      from_date: dateRange.fromDate || '',
      to_date: dateRange.toDate || '',
    });

    this.drawer && this.drawer.toggle();
  };

  getItemList = () => {
    if (this.props.searchResults.length > 0) {
      return this.props.searchResults;
    }

    if (this.props.isLoadingPage) {
      return [...this.props.list, {isLoadingPage: true}];
    }

    return this.props.list;
  };

  showDetailsWithId = item_id => () => {
    this.props.navigation.navigate(screenIds.ITEM_DETAILS, {
      item_id,
    });
  };

  renderDrawerContent = () => {
    return (
      <Filter
        byChannel={this.props.filterByChannel}
        byDate={this.props.filterByDate}
        onFilter={this.filter}
      />
    );
  };

  renderItem = ({item = {}, index} = {}) => {
    return (
      <ActivityListItem
        key={index}
        data={item}
        onPress={this.showDetailsWithId(item.id)}
        onGoingPress={this.proceedToggleGoing(item.id)}
      />
    );
  };

  renderSearchResult = () => {
    return (
      <View style={styles.searchResult}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <Text style={{color: themeColor.primary, fontSize: 20}}>
            {this.props.searchResults.length} Result
          </Text>
          <TouchableOpacity onPress={this.props.clearSearch}>
            <View
              style={{
                height: 24,
                backgroundColor: themeColor.selection,
                paddingHorizontal: 14,
                borderRadius: 12,
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: 10, color: themeColor.primaryDark}}>
                CLEAR
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        {this.filterChannel.label && this.filterDate.label && (
          <Text>
            {`Searched for Channel ${this.filterChannel.label || ''}` +
              ` Activities on ${this.filterDate.label || ''}`}
          </Text>
        )}
      </View>
    );
  };

  render() {
    const noResultFound =
      !this.props.searchFound && this.props.searchResults.length === 0;
    return (
      <Drawer
        ref={ref => (this.drawer = ref)}
        renderDrawerContent={this.renderDrawerContent}>
        <View style={styles.drawer}>
          <Header
            onLeftPress={() => this.drawer && this.drawer.toggle()}
            onRightPress={() =>
              this.props.navigation.navigate(screenIds.USER_PROFILE)
            }
          />
          {(this.props.searchResults.length > 0 || noResultFound) &&
            this.renderSearchResult()}
          <ActivityList
            renderItem={this.renderItem}
            activities={noResultFound ? [] : this.getItemList()}
            loadNextPage={this.loadNextPage}
            isWaiting={this.props.isSearching}
          />
        </View>
      </Drawer>
    );
  }
}

const styles = StyleSheet.create({
  searchResult: {
    width: '100%',
    height: 68,
    backgroundColor: '#FAF9FC',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  drawer: {
    flex: 1,
    backgroundColor: themeColor.primary,
  },
  drawerContent: {
    flex: 1,
    backgroundColor: themeColor.primaryDark,
    flexDirection: 'column',
  },
});

const mapStateToProps = state => ({
  userInfo: state.login.userInfo,
  list: state.listing.list.data,
  filterByDate: state.listing.filters.by_date,
  filterByChannel: state.listing.filters.by_channel,
  searchResults: state.listing.search.data,
  searchFound: state.listing.search.found,
  isSearching: state.listing.search.loading,
  isLoadingPage: state.listing.list.loading,
  pageIndex: state.listing.search.page_index,
});

export default connect(
  mapStateToProps,
  {
    logout,
    retrieveFilters,
    retrieveList,
    toggleGoing,
    searchItems,
    clearSearch,
  },
)(Listing);
