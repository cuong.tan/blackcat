const toRequest = type => `${type}_REQUEST`;
const toSuccess = type => `${type}_SUCCESS`;
const toFailure = type => `${type}_FAILURE`;

const makeQueryString = (params = {}) => {
  if (!params || Object.keys(params).length === 0) {
    return '';
  }

  return (
    '?' +
    Object.entries(params)
      .map(([key, value]) => `${key}=${value}`)
      .join('&')
  );
};

const createAsyncAction = ({
  api = '',
  type = '',
  method = 'GET',
  params = {},
  onSuccess = () => {},
  onFailure = () => {},
  ...rest
} = {}) => async (dispatch, getState) => {
  try {
    dispatch({
      type: toRequest(type),
      payload: {},
    });

    const response = await fetch(
      `http://0.0.0.0:3000/${api}${makeQueryString(params)}`,
      {
        method,
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        ...rest,
      },
    );

    const jsonResponse = await response.json();

    onSuccess({response: jsonResponse, dispatch, getState});

    dispatch({
      type: toSuccess(type),
      payload: {
        ...jsonResponse,
      },
    });
  } catch (e) {
    onFailure({error: e, dispatch, getState});

    dispatch({
      type: toFailure(type),
      payload: {
        error: JSON.stringify(e),
      },
    });
  }
};

export {createAsyncAction, toRequest, toSuccess, toFailure};
