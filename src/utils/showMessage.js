import {Alert} from 'react-native';

const showMessage = (message, title) => {
  Alert.alert(title, message, [
    {text: 'Close', onPress: () => {}, style: 'cancel'},
  ]);
};

export {showMessage};
