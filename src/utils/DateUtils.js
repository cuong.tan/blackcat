const formatWithValidDate = (value = '') => {
  let day = '',
    month = '',
    year = '';

  for (let i = 0; i < value.length; i++) {
    if (!Number.isInteger(value[i] - '0')) {
      continue;
    }

    if (i < 2) {
      day += value[i];
    } else if (i < 5) {
      month += value[i];
    } else {
      year += value[i];
    }
  }

  return [day, month, year].filter(str => str.trim().length > 0).join('/');
};

const isLeapYear = year => {
  return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
};

/* date must be DD/MM/YYYY */
const isValidDate = (date = '') => {
  const [day, month, year] = date.split('/').map(Number.parseInt);
  const daysOfMonth = [0, 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  if (day <= 0) {
    return false;
  }

  if (month === 2) {
    return isLeapYear(year) ? day <= 29 : day <= 28;
  } else {
    return day <= daysOfMonth[month];
  }
};

export {formatWithValidDate, isValidDate};
